var path = require("path");
const proxy = require('http-proxy-middleware');

const HtmlWebpackPlugin = require('html-webpack-plugin');
var config = {
    entry: {app: "./src/app.tsx"},
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".css", ".less", ".json"]
    },
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        host: "localhost",
        setup: function (app) {
            app.use(proxy('/api', {target: 'http://olymp.alesharik.com:8080/'}));
        },
        historyApiFallback: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Development',
            template: './index.html',
            filename: './index.html'

        })
    ],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "less-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=512&&name=[path][name].[ext]?[hash]'
            }

        ]
    }
};

module.exports = config;

