import * as React from 'react';
import {Component} from 'react';
import {Button, Dimmer, Input, Loader, Message, Modal} from "semantic-ui-react";
import ImageUploader from 'react-images-upload'
import {api} from "../app";

export class UploadModalProps {
    public open: boolean;
    public onClose: () => void;

    constructor(open: boolean, onClose: () => void) {
        this.open = open;
        this.onClose = onClose;
    }
}

class UploadModalState {
    public file: File;
    public name: string;
    public loading: boolean;
    public error: string;

    constructor(file: File, name: string, loading: boolean, error: string) {
        this.file = file;
        this.name = name;
        this.loading = loading;
        this.error = error;
    }
}

export class UploadModal extends Component<UploadModalProps, UploadModalState> {
    constructor(props: UploadModalProps, context: any) {
        super(props, context);
        this.state = new UploadModalState(undefined, "", false, "");
        this.onFileSelectChange = this.onFileSelectChange.bind(this);
        this.onNameChanged = this.onNameChanged.bind(this);
        this.submit = this.submit.bind(this);
    }

    onFileSelectChange(files: File[], urls: []) {
        if(files.length >= 2)
            files.splice(0, files.length - 1);
        if(urls.length >= 2)
            urls.splice(0, urls.length - 1);
        this.setState(new UploadModalState(files[0], this.state.name, this.state.loading, this.state.error));
    }

    onNameChanged(e, {value}) {
        this.setState(new UploadModalState(this.state.file, value, this.state.loading, this.state.error));
    }

    submit() {
        api.upload(this.state.name, this.state.file).then(value => {
            this.props.onClose();
            this.setState(new UploadModalState(this.state.file, this.state.name, false, ""));
        }).catch(reason => {
            this.setState(new UploadModalState(this.state.file, this.state.name, false, reason.toString()));
        });
        this.setState(new UploadModalState(this.state.file, this.state.name, true, this.state.error));
    }

    render() {
        return <Modal open={this.props.open} onClose={this.props.onClose}>
            <Modal.Header>Upload image</Modal.Header>
            <Modal.Content>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <ImageUploader singleImage={true}
                               withIcon={true}
                               buttonText={"Upload image"}
                    // @ts-ignore
                               onChange={this.onFileSelectChange}
                               withPreview={true}/>
                <Modal.Description>
                    <Input onChange={this.onNameChanged} placeholder={"Name"} value={this.state.name}/>
                    {this.state.error ? <Message error header={"Upload error"} content={this.state.error}/> : <div/>}
                    <Button primary onClick={this.submit}
                            disabled={!this.state.name || this.state.name.length == 0 || !this.state.file}>Upload</Button>
                </Modal.Description>
            </Modal.Content>
        </Modal>
    }
}