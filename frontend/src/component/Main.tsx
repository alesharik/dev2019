import * as React from 'react';
import {history} from 'history';
import {WiredAppMenu} from "./WiredAppMenu";
import {UploadModal} from "./UploadModal";
import {Api, Image} from "../api/Api";
import {api} from "../app";
import {Dimmer, Dropdown, Label, Loader, Rating, Segment} from "semantic-ui-react";
import * as Gallery from "react-grid-gallery";

export class MainProps {
    public history: history;

    constructor(history: history) {
        this.history = history;
    }
}

class DisplayedImage {
    public image: Image;
    public src: string;
    public thumbnail: string;
    public thumbnailWidth: number;
    public thumbnailHeight: number;
    public caption: string;
    public tags: {}[];

    constructor(image: Image) {
        this.image = image;
        this.src = Api.getImageUrl(image.id);
        this.thumbnail = this.src;
        this.thumbnailHeight = image.height;
        this.thumbnailWidth = image.width;
        this.tags = [];
        this.caption = image.name;
        image.tags.forEach(value => {
            this.tags.push({value: value, title: value});
        })
    }
}

class MainState {
    public uploadOpen: boolean;
    public images: DisplayedImage[];
    public loading: boolean;
    public currentImage: number;
    public search: string[];
    public nameSort: string;
    public ratingSort: string;

    constructor(uploadOpen: boolean, images: DisplayedImage[], loading: boolean, currentImage: number, search: string[], nameSort: string, ratingSort: string) {
        this.uploadOpen = uploadOpen;
        this.images = images;
        this.loading = loading;
        this.currentImage = currentImage;
        this.search = search;
        this.nameSort = nameSort;
        this.ratingSort = ratingSort;
    }
}

export class Main extends React.Component<MainProps, MainState> {
    constructor(props: MainProps, context: any) {
        super(props, context);
        this.state = new MainState(false, [], true, 0, [], "NONE", "NONE");
        this.openUpload = this.openUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
        this.onImageWillChange = this.onImageWillChange.bind(this);
        this.onTagAdded = this.onTagAdded.bind(this);
        this.onTagChange = this.onTagChange.bind(this);
        this.searchUpdate = this.searchUpdate.bind(this);
        this.rate = this.rate.bind(this);
        this.viewUpdateRating = this.viewUpdateRating.bind(this);
        this.viewUpdateName = this.viewUpdateName.bind(this);
        let that = this;
        // @ts-ignore
        Api.queryImages(0, -1, this.state.search, this.state.nameSort, this.state.ratingSort)
            .then(value => {
                that.setState(new MainState(this.state.uploadOpen, value.map(value1 => new DisplayedImage(value1)), false, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
            })
    }

    openUpload() {
        this.setState(new MainState(true, this.state.images, this.state.loading, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
    }

    closeUpload() {
        this.setState(new MainState(false, this.state.images, this.state.loading, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
        // @ts-ignore
        Api.queryImages(0, -1, this.state.search, this.state.nameSort, this.state.ratingSort)
            .then(value => {
                this.setState(new MainState(this.state.uploadOpen, value.map(value1 => new DisplayedImage(value1)), false, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
            })
    }

    onImageWillChange(index) {
        this.setState(new MainState(false, this.state.images, this.state.loading, index, this.state.search, this.state.nameSort, this.state.ratingSort));
    }

    onTagChange(e, {value}) {
        this.setState(new MainState(this.state.uploadOpen, this.state.images, true, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
        let image = this.state.images[this.state.currentImage].image;
        let tags = image.tags.slice(0);
        let promises = [];
        value.forEach((e) => {
            let idx = tags.indexOf(e);
            if (idx == -1) {
                api.addTag(e);
                promises.push(api.putTagOnImage(image, e));
            } else
                tags.splice(idx, 1);
        });
        tags.forEach(value1 => {
            promises.push(api.removeTagFromImage(image, value1))
        });
        const end = () => {
            this.state.images[this.state.currentImage].image.tags = value;
            this.state.images[this.state.currentImage].tags = value.map((e) => {
                return {value: e, title: e};
            });
            this.setState(new MainState(this.state.uploadOpen, this.state.images, false, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
        };
        if (promises.length > 0)
            Promise.all(promises).then(end);
        else
            end();
    }

    onTagAdded(e, {value}) {
        api.addTag(e);
        this.state.images[this.state.currentImage].image.tags.push(value);
    }

    searchUpdate(search: string[]) {
        // @ts-ignore
        Api.queryImages(0, -1, search, this.state.nameSort, this.state.ratingSort)
            .then(value => {
                this.setState(new MainState(this.state.uploadOpen, value.map(value1 => new DisplayedImage(value1)), false, this.state.currentImage, search, this.state.nameSort, this.state.ratingSort));
            })
    }

    rate(e, {rating}) {
        if(rating == 0) {
            api.unVoteForImage(this.state.images[this.state.currentImage].image).then(() => {
                this.setState(new MainState(this.state.uploadOpen, this.state.images, this.state.loading, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
            })
        } else {
            api.voteForImage(this.state.images[this.state.currentImage].image, rating).then(() => {
                this.setState(new MainState(this.state.uploadOpen, this.state.images, this.state.loading, this.state.currentImage, this.state.search, this.state.nameSort, this.state.ratingSort));
            })
        }
    }

    viewUpdateName(sort: string) {
        // @ts-ignore
        Api.queryImages(0, -1, this.state.search, sort, this.state.ratingSort)
            .then(value => {
                this.setState(new MainState(this.state.uploadOpen, value.map(value1 => new DisplayedImage(value1)), false, this.state.currentImage, this.state.search, sort, this.state.ratingSort));
            })
    }

    viewUpdateRating(sort: string) {
        // @ts-ignore
        Api.queryImages(0, -1, this.state.search, this.state.nameSort, sort)
            .then(value => {
                this.setState(new MainState(this.state.uploadOpen, value.map(value1 => new DisplayedImage(value1)), false, this.state.currentImage, this.state.search, this.state.nameSort, sort));
            })
    }

    render() {
        return (
            <div>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <WiredAppMenu search={this.state.search} onSearchChange={search => {
                    this.setState(new MainState(this.state.uploadOpen, this.state.images, true, this.state.currentImage, search, this.state.nameSort, this.state.ratingSort));
                    this.searchUpdate(search);
                }} history={this.props.history} onUploadClicked={this.openUpload} ratingSort={this.state.ratingSort} nameSort={this.state.nameSort}
                onRatingSortChange={sort => {
                    this.setState(new MainState(this.state.uploadOpen, this.state.images, true, this.state.currentImage, this.state.search, this.state.nameSort, sort));
                    this.viewUpdateRating(sort);
                }}
                              onNameSortChange={sort => {
                    this.setState(new MainState(this.state.uploadOpen, this.state.images, true, this.state.currentImage, this.state.search, sort, this.state.ratingSort));
                    this.viewUpdateName(sort);
                }}/>
                <UploadModal open={this.state.uploadOpen} onClose={this.closeUpload}/>
                <Gallery images={this.state.images} enableImageSelection={false} backdropClosesModal={true}
                         currentImageWillChange={this.onImageWillChange}
                         customControls={[<Segment style={{marginLeft: "-25vw", marginTop: "30%", height: "10vh", minHeight: "100px", maxWidth: "20vw"}} key={this.state.currentImage + "-div"}>
                                 <Dropdown key={this.state.currentImage} placeholder={"Tags..."}
                                           search
                                           selection
                                           fluid
                                           multiple
                                           allowAdditions
                                           options={api.tags.map(value => {
                                               return {key: value, text: value, value: value}
                                           })}
                                           disabled={!api.isLoggedIn}
                                     // @ts-ignore
                                           value={this.state.images.length == 0 ? [] : this.state.images[this.state.currentImage].image.tags}
                                           onChange={this.onTagChange}
                                           onAddItem={this.onTagAdded}
                                 />
                             {api.isLoggedIn ? <Rating key={this.state.currentImage + "-rating"} icon={"star"} maxRating={5}
                                                       defaultRating={this.state.images.length == 0 ? 0 : this.state.images[this.state.currentImage].image.myVote}
                                                       onRate={this.rate}/> : <div/>}
                             </Segment>
                         ]}/>
            </div>
        )
    }
}