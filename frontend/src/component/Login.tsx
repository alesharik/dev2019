import * as React from "react";
import {Container, Form, Grid, Message} from "semantic-ui-react";
import {api} from "../app";

class LoginState {
    public login: string;
    public password: string;
    public ok: boolean;
    public loading: boolean;
    public error: string;

    constructor(login: string, password: string, ok: boolean, loading: boolean, error: string) {
        this.login = login;
        this.password = password;
        this.ok = ok;
        this.loading = loading;
        this.error = error;
    }
}

export class Login extends React.Component<any, LoginState> {

    constructor(props: any, context: any) {
        super(props, context);
        this.state = new LoginState("", "", true, false, "");
        this.updateLogin = this.updateLogin.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
        this.submit = this.submit.bind(this);
    }

    updateLogin(e, {value}) {
        this.setState(new LoginState(value, this.state.password, this.state.ok, false, ""));
    }

    updatePassword(e, {value}) {
        this.setState(new LoginState(this.state.login, value, this.state.ok, false, ""));
    }

    submit() {
        // noinspection JSIgnoredPromiseFromCall
        api.login(this.state.login, this.state.password)
            .then(() => {
                this.props.history.push("/")
            }).catch(reason => {
                this.setState(new LoginState(this.state.login, this.state.password, false, true, reason.toString()));
            });
        this.setState(new LoginState(this.state.login, this.state.password, this.state.ok, true, ""));
    }

    render() {
        return <Container><Grid className={"segment centered"}>
            <Form error={!this.state.ok}>
                <Form.Field>
                    <Form.Input placeholder={"Login"} onChange={this.updateLogin} value={this.state.login}/>
                </Form.Field>
                <Form.Field>
                    <Form.Input placeholder={"Password"} onChange={this.updatePassword} value={this.state.password}
                                type={"password"}/>
                </Form.Field>
                {this.state.ok ? <div/> : <Message error header={"Login failed"} content={this.state.error}/>}
                <Form.Button content={"Login"} onClick={this.submit}/>
            </Form>
        </Grid></Container>;
    }
}