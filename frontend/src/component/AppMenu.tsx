import * as React from 'react';
import {Button, Dropdown, Menu} from "semantic-ui-react";
import {history} from 'history';
import {api} from "../app";

export class AppMenuProps {
    public searchTags: string[];
    public onSearchChange: (search: string[]) => void;
    public onNameSortChange: (sort: string) => void;
    public onRatingSortChange: (sort: string) => void;
    public nameSort: string;
    public ratingSort: string;
    public onUploadClicked: () => void;
    public isLogged: boolean;
    public login: string;
    public loading: boolean;
    public history: history;
    public search: string[];

    constructor(searchTags: string[], onSearchChange: (search: string[]) => void, onUploadClicked: () => void, isLogged: boolean, login: string, loading: boolean, history: History, search: string[], onNameSortChange: (sort: string) => void, onRatingSortChange: (sort: string) => void, nameSort: string, ratingSort: string) {
        this.searchTags = searchTags;
        this.onSearchChange = onSearchChange;
        this.onUploadClicked = onUploadClicked;
        this.isLogged = isLogged;
        this.login = login;
        this.loading = loading;
        this.history = history;
        this.search = search;
        this.onNameSortChange = onNameSortChange;
        this.onRatingSortChange = onRatingSortChange;
        this.nameSort = nameSort;
        this.ratingSort = ratingSort;
    }
}

export class AppMenu extends React.Component<AppMenuProps> {
    constructor(props: AppMenuProps, context: any) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e, {value}) {
        this.props.onSearchChange(value);
    }

    render() {
        // noinspection SillyAssignmentJS
        return <Menu inverted className={"attached"}>
            <Menu.Item style={{minWidth: "40%"}}>
                <Dropdown placeholder={"Search..."} loading={this.props.loading} fluid multiple search selection
                          options={this.props.searchTags.map(value => {
                              return {key: value, text: value, value: value}
                          })} value={this.props.search} onChange={this.handleChange}/>
            </Menu.Item>
            <Menu.Item>
                <span>
                    Sort by name:
                    <Dropdown inverted={true} value={this.props.nameSort} options={[{key: "DESCEND", value: "DESCEND", text: "DESCEND"}, {key: "ASCEND", value: "ASCEND", text: "ASCEND"}, {key: "NONE", value: "NONE", text: "NONE"}]} onChange={(e, {value}) => this.props.onNameSortChange(value as string)}/>;
                </span>
                <span>
                    Sort by rating:
                    <Dropdown inverted={true} value={this.props.ratingSort} options={[{key: "DESCEND", value: "DESCEND", text: "DESCEND"}, {key: "ASCEND", value: "ASCEND", text: "ASCEND"}, {key: "NONE", value: "NONE", text: "NONE"}]} onChange={(e, {value}) => this.props.onRatingSortChange(value as string)}/>;
                </span>
            </Menu.Item>
            {this.props.isLogged ? <Button primary inverted onClick={this.props.onUploadClicked}>Upload</Button> :
                <div/>}
            <Menu.Item position={"right"}>
                {this.props.isLogged ? <Dropdown pointing text={this.props.login}>
                    <Dropdown.Menu>
                        <Dropdown.Item
                            onClick={() => api.logout().then(() => window.location.href = window.location.href)}>
                            Logout
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown> : <span>
                        <Button primary={true} inverted
                                onClick={() => this.props.history.push("/login")}>Login</Button>
                        <Button inverted onClick={() =>
                            this.props.history.push("/register")}>Register</Button>
                </span>}
            </Menu.Item>
        </Menu>;
    }
}