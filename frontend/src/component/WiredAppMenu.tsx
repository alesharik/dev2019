import * as React from 'react';
import {Api} from "../api/Api";
import {AppMenu} from "./AppMenu";
import {history} from "history";
import {api} from "../app";

export class WiredAppMenuProps {
    public onSearchChange: (search: string[]) => void;
    public onUploadClicked: () => void;
    public history: history;
    public search: string[];
    public onNameSortChange: (sort: string) => void;
    public onRatingSortChange: (sort: string) => void;
    public nameSort: string;
    public ratingSort: string;

    constructor(api: Api, onSearchChange: (search: string[]) => void, onUploadClicked:() => void, history: history, search: [], onNameSortChange: (sort: string) => void, onRatingSortChange: (sort: string) => void, nameSort: string, ratingSort: string) {
        this.onSearchChange = onSearchChange;
        this.onUploadClicked = onUploadClicked;
        this.history = history;
        this.search = search;
        this.onNameSortChange = onNameSortChange;
        this.onRatingSortChange = onRatingSortChange;
        this.nameSort = nameSort;
        this.ratingSort = ratingSort;
    }
}

class WiredAppMenuState {
    tags: string[];
    loading: boolean;

    constructor(tags: string[], loading: boolean) {
        this.tags = tags;
        this.loading = loading;
    }
}

export class WiredAppMenu extends React.Component<WiredAppMenuProps, WiredAppMenuState> {
    constructor(props: WiredAppMenuProps, context: any) {
        super(props, context);
        this.state = new WiredAppMenuState([], true);
        Api.getAllTags().then(value => this.setState(new WiredAppMenuState(value, false)));
    }

    render() {
        return (<AppMenu searchTags={this.state.tags} search={this.props.search} nameSort={this.props.nameSort} ratingSort={this.props.ratingSort} onNameSortChange={this.props.onNameSortChange} onRatingSortChange={this.props.onRatingSortChange} onUploadClicked={this.props.onUploadClicked} onSearchChange={this.props.onSearchChange} isLogged={api.isLoggedIn} login={api.isLoggedIn ? api.me.login : undefined} loading={this.state.loading} history={this.props.history}/>);
    }
}