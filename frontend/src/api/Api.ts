export class User {
    public id: number;
    public login: string;
}

export class Vote {
    public user: User;
    public value: number;

    constructor(user: User, value: number) {
        this.user = user;
        this.value = value;
    }
}

export class Image {
    public id: number;
    public name: string;
    public tags: string[];
    public user: User;
    public votes: Vote[];
    public width: number;
    public height: number;
    public myVote: number | -1;
}

class ImageQuery {
    public tags: string[];
    public nameSort: "DESCEND" | "ASCEND"|"NONE";
    public voteSort: "DESCEND" | "ASCEND"|"NONE";
    public start: number;
    public limit: number;
}

export const errorHandlers: ((reason: string) => void)[] = [];

export class Api {
    public isLoggedIn: boolean;
    public me: User;
    public tags: string[];

    constructor() {
        this.isLoggedIn = false;
        this.tags = [];
        fetch("/api/user/info").then(v => {
            if(v.status != 200 || v.redirected)
                return;
            v.json().then(v1 => {
                this.isLoggedIn = true;
                this.me = v1;
            })
        });
        Api.getAllTags().then(value => this.tags = value);
    }

    static getAllTags(): Promise<string[]> {
        return fetch("/api/public/tags").then(value => {
            if(!value.ok)
                throw new Error(value.statusText);
            return value;
        }).then(value => value.json());
    }

    static getImageUrl(id: number): string {
        return "/api/public/image/data?id=" + id;
    }

    static queryImages(start: number, limit: number, tags?: string[], nameSort?: "DESCEND" | "ASCEND", voteSort?: "DESCEND" | "ASCEND"): Promise<Image[]> {
        let query = new ImageQuery();
        query.limit = limit;
        query.start = start;
        query.tags = tags ? tags : [];
        query.nameSort = nameSort ? nameSort : "ASCEND";
        query.voteSort = voteSort ? voteSort : "DESCEND";
        let json = JSON.stringify(query);
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        return fetch("/api/public/image/query", {method: "POST", body: json, headers: headers})
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return value;
            })
            .then(value => value.json())
            .catch(reason => {
                errorHandlers.forEach(value => value(reason));
                return [];
            })
    }

    static register(login: string, password: string): Promise<undefined> {
        return fetch("/api/register?login=" + login + "&password=" + password)
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return undefined;
            })
    }

    addTag(tag: string) {
        if(this.tags.indexOf(tag) == -1)
            this.tags.push(tag);
    }

    login(login: string, password: string): Promise<undefined> {
        return fetch("/api/login?username=" + login + "&password=" + password)
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return fetch("/api/user/info").then(v => {
                    return v.json().then(v1 => {
                        this.me = v1;
                        this.isLoggedIn = true;
                        return undefined;
                    })
                });
            })
    }

    logout() {
        return fetch("/api/logout")
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
            }).catch(reason => {
                errorHandlers.forEach(value => value(reason));
            });
    }

    upload(name: string, file: File): Promise<undefined> {
        return fetch("/api/image/upload?name=" + name, {method: "PUT", body: file})
            .then(value => {
                if(!value.ok)
                    throw new Error(value.statusText);
                return undefined;
            });
    }

    putTagOnImage(image: Image, tag: string): Promise<undefined | Image> {
        return fetch("/api/image/tags/add?image=" + image.id + "&tag=" + tag, {method: "PUT"})
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                image.tags.push(tag);
                return image;
            })
            .catch(reason => {
                errorHandlers.forEach(value => value(reason));
                return undefined;
            });
    }

    removeTagFromImage(image: Image, tag: string): Promise<undefined | Image> {
        return fetch("/api/image/tags/remove?image=" + image.id + "&tag=" + tag, {method: "DELETE"})
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                image.tags.splice(image.tags.indexOf(tag), 1);
                Api.getAllTags().then(value => this.tags = value);
                return image;
            })
            .catch(reason => {
                errorHandlers.forEach(value => value(reason));
                return undefined;
            });
    }

    voteForImage(image: Image, vote: number): Promise<undefined | Image> {
        return fetch("/api/image/vote?image=" + image.id + "&vote=" + vote, {method: "PUT"})
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                image.myVote = vote;
                image.votes.push(new Vote(this.me, vote));
                return image;
            })
            .catch(reason => {
                errorHandlers.forEach(value => value(reason));
                return undefined;
            });
    }

    unVoteForImage(image: Image) {
        return fetch("/api/image/vote?image=" + image.id, {method: "DELETE"})
            .then((value) => {
                if(!value.ok)
                    throw new Error(value.statusText);
                image.myVote = -1;
                image.votes = image.votes.filter(value => value.user.id != this.me.id);
                return image;
            })
            .catch(reason => {
                errorHandlers.forEach(value => value(reason));
                return undefined;
            });
    }
}