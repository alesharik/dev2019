package com.alesharik.web.rest;

import com.alesharik.web.auth.UserDetailsImpl;
import com.alesharik.web.data.Image;
import com.alesharik.web.data.ImageData;
import com.alesharik.web.data.ImageDataRepository;
import com.alesharik.web.data.ImageRepository;
import com.alesharik.web.data.json.ImageQuery;
import com.alesharik.web.data.json.JsonImage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ImageController {
    private final ImageRepository repository;
    private final ImageDataRepository imageData;

    @PostMapping("/public/image/query")
    public ResponseEntity<List<JsonImage>> getImages(@RequestBody ImageQuery query) {
        List<Image> images = repository.queryImages(query);
        return new ResponseEntity<>(images.stream()
                .map(JsonImage::new)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @PutMapping("/image/upload")
    public ResponseEntity<JsonImage> upload(@RequestParam("name") String name, @RequestBody byte[] body) throws IOException {
        BufferedImage in = ImageIO.read(new ByteArrayInputStream(body));
        BufferedImage img = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        img.getGraphics().drawImage(in, 0, 0, null);
        img.getGraphics().dispose();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write(img, "jpg", out);
        ImageData dat = new ImageData(out.toByteArray());
        dat = imageData.save(dat);

        SecurityContext context = SecurityContextHolder.getContext();
        UserDetailsImpl details = (UserDetailsImpl) context.getAuthentication().getPrincipal();
        Image image = new Image();
        image.setName(name);
        image.setUser(details.getUser());
        image.setData(dat);
        image.setWidth(in.getWidth());
        image.setHeight(in.getHeight());
        image.setTags(Collections.emptyList());
        image.setVotes(Collections.emptyList());
        return new ResponseEntity<>(new JsonImage(repository.save(image)), HttpStatus.OK);
    }
}
