package com.alesharik.web.rest;

import com.alesharik.web.data.Image;
import com.alesharik.web.data.ImageRepository;
import com.alesharik.web.data.ImageTag;
import com.alesharik.web.data.ImageTagRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class TagsController {
    private final ImageTagRepository tags;
    private final ImageRepository images;

    @PutMapping("/image/tags/add")
    public void addTag(@RequestParam("image") long imageId, @RequestParam("tag") String tag) {
        Image image = images.findById(imageId).orElseThrow(() -> new IllegalArgumentException("Image not found"));
        ImageTag imageTag = tags.findByName(tag).orElseGet(() -> {
            ImageTag t = new ImageTag();
            t.setName(tag);
            t.setImages(new ArrayList<>());
            return tags.save(t);
        });
        Collection<ImageTag> imageTags = image.getTags();
        if(imageTags.contains(imageTag))
            return;
        imageTags.add(imageTag);
        images.save(image);
    }

    @DeleteMapping("/image/tags/remove")
    public void removeTag(@RequestParam("image") long imageId, @RequestParam("tag") String tagName) {
        Image image = images.findById(imageId).orElseThrow(() -> new IllegalArgumentException("Image not found"));
        ImageTag tag = tags.findByName(tagName).orElseThrow(() -> new IllegalArgumentException("Tag not found"));
        image.getTags().remove(tag);
        images.save(image);
        tag.getImages().remove(image);
        tag = tags.save(tag);
        if(tag.getImages().isEmpty())
            tags.delete(tag);
    }

    @GetMapping("/public/tags")
    public ResponseEntity<List<String>> getTags() {
        Iterable<ImageTag> all = tags.findAll();
        Stream.Builder<ImageTag> builder = Stream.builder();
        all.forEach(builder::add);
        return new ResponseEntity<>(builder.build()
                .map(ImageTag::getName)
                .collect(Collectors.toList()), HttpStatus.OK);
    }
}
