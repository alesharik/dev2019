package com.alesharik.web.rest;

import com.alesharik.web.data.Image;
import com.alesharik.web.data.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public/image")
@RequiredArgsConstructor
public class ImageDataController {
    private final ImageRepository images;

    @GetMapping(value = "/data", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] get(@RequestParam("id") long id) {
        Image image = images.findById(id).orElseThrow(() -> new IllegalArgumentException("Image not found"));
        return image.getData().getData();
    }
}
