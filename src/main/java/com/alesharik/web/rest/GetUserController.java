package com.alesharik.web.rest;

import com.alesharik.web.auth.UserDetailsImpl;
import com.alesharik.web.data.json.JsonUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GetUserController {

    @GetMapping("/user/info")
    public ResponseEntity<JsonUser> getLogin() {
        SecurityContext context = SecurityContextHolder.getContext();
        UserDetailsImpl details = (UserDetailsImpl) context.getAuthentication().getPrincipal();
        return new ResponseEntity<>(new JsonUser(details.getUser()), HttpStatus.OK);
    }
}
