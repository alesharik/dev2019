package com.alesharik.web.rest;

import com.alesharik.web.auth.UserDetailsImpl;
import com.alesharik.web.data.Image;
import com.alesharik.web.data.ImageRepository;
import com.alesharik.web.data.ImageVote;
import com.alesharik.web.data.ImageVoteRepository;
import com.alesharik.web.data.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class VoteController {
    private final ImageRepository images;
    private final ImageVoteRepository votes;

    @PutMapping("/vote")
    public void vote(@RequestParam("image") long imageId, @RequestParam("vote") int vote) {
        if(vote < 1 || vote > 5)
            throw new IllegalArgumentException("Vote out of range");
        SecurityContext context = SecurityContextHolder.getContext();
        UserDetailsImpl details = (UserDetailsImpl) context.getAuthentication().getPrincipal();
        User user = details.getUser();
        Image image = images.findById(imageId).orElseThrow(() -> new IllegalArgumentException("Image not found"));
        for(ImageVote imageVote : image.getVotes()) {
            if(imageVote.getUser().getId() == user.getId()) {
                imageVote.setValue(vote);
                votes.save(imageVote);
                return;
            }
        }
        ImageVote v = new ImageVote();
        v.setImage(image);
        v.setUser(user);
        v.setValue(vote);
        v = votes.save(v);
        image.getVotes().add(v);
        images.save(image);
    }

    @DeleteMapping("/vote")
    public void deleteVote(@RequestParam("image") long imageId) {
        SecurityContext context = SecurityContextHolder.getContext();
        UserDetailsImpl details = (UserDetailsImpl) context.getAuthentication().getPrincipal();
        User user = details.getUser();
        Image image = images.findById(imageId).orElseThrow(() -> new IllegalArgumentException("Image not found"));
        for(ImageVote vote : image.getVotes()) {
            if(vote.getUser().getId() == user.getId()) {
                image.getVotes().remove(vote);
                images.save(image);
                votes.delete(vote);
                return;
            }
        }
        throw new IllegalStateException("Vote not found");
    }
}
