package com.alesharik.web.data;

import com.alesharik.web.data.json.ImageQuery;

import java.util.List;

public interface ImageQueryRepository {
    List<Image> queryImages(ImageQuery query);
}
