package com.alesharik.web.data.json;

import com.alesharik.web.data.ImageVote;
import lombok.Data;

@Data
public class JsonImageVote {
    private JsonUser user;
    private int value;

    public JsonImageVote(ImageVote note) {
        this.user = new JsonUser(note.getUser());
        this.value = note.getValue();
    }
}
