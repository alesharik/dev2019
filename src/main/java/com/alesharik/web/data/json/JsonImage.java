package com.alesharik.web.data.json;

import com.alesharik.web.data.Image;
import com.alesharik.web.data.ImageTag;
import com.alesharik.web.data.ImageVote;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class JsonImage {
    private final long id;
    private final String name;
    private final List<String> tags;
    private final List<JsonImageVote> votes;
    private final JsonUser user;
    private final int myVote;
    private final int width;
    private final int height;

    public JsonImage(Image image) {
        this.id = image.getId();
        this.name = image.getName();
        this.tags = image.getTags().stream()
                .map(ImageTag::getName)
                .collect(Collectors.toList());
        this.user = new JsonUser(image.getUser());
        this.votes = new ArrayList<>();
        int myNote = -1;
        for(ImageVote vote : image.getVotes()) {
            this.votes.add(new JsonImageVote(vote));
            if(vote.getUser().getId() == user.getId())
                myNote = vote.getValue();
        }
        this.myVote = myNote;
        this.width = image.getWidth();
        this.height = image.getHeight();
    }
}
