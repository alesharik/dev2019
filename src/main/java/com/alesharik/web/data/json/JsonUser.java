package com.alesharik.web.data.json;

import com.alesharik.web.data.User;
import lombok.Data;

@Data
public class JsonUser {
    private final long id;
    private final String login;

    public JsonUser(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
    }
}
