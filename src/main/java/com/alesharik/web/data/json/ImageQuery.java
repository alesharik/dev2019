package com.alesharik.web.data.json;

import lombok.Data;

import java.util.List;

@Data
public class ImageQuery {
    private final List<String> tags;
    private final SortType nameSort;
    private final SortType voteSort;
    private final int start;
    private final int limit;

    public enum SortType {
        DESCEND,
        ASCEND,
        NONE
    }
}
