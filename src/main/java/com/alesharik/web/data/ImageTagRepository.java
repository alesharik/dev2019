package com.alesharik.web.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ImageTagRepository extends CrudRepository<ImageTag, Long> {
    @Query("select tag from ImageTag tag where tag.name = :name")
    Optional<ImageTag> findByName(@Param("name") String name);
}
