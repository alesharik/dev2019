package com.alesharik.web.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageDataRepository extends CrudRepository<ImageData, Long> {
}
