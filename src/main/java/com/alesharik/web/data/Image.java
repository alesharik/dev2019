package com.alesharik.web.data;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Data
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 128)
    private String name;

    @ManyToOne
    private User user;

    @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
    private ImageData data;

    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<ImageTag> tags;

    @OneToMany(orphanRemoval = true)
    private Collection<ImageVote> votes;

    private int width;
    private int height;
}
