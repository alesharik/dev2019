package com.alesharik.web.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageVoteRepository extends CrudRepository<ImageVote, Long> {
}
