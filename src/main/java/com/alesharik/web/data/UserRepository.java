package com.alesharik.web.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Long> {
    @Query("select count(user) from User user where user.login = :login")
    long countUserByLogin(@Param("login") String login);
}
