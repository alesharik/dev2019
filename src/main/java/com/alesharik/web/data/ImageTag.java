package com.alesharik.web.data;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Data
@Table(name = "image_tags", indexes = {@Index(columnList = "name")})
public class ImageTag {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 128, unique = true)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Image> images;
}
