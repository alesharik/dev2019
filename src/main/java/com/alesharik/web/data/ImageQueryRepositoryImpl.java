package com.alesharik.web.data;

import com.alesharik.web.data.json.ImageQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class ImageQueryRepositoryImpl implements ImageQueryRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Image> queryImages(ImageQuery query) {
        StringBuilder q = new StringBuilder("SELECT *, (SELECT SUM(image_vote.value) FROM image_vote WHERE image_vote.image_id = images.id) as v FROM images ");
        List<String> tags = query.getTags();
        if(!tags.isEmpty())
            q.append(" WHERE ");
        for(int i = 0; i < tags.size(); i++) {
            q.append("    EXISTS(SELECT * FROM images_tags INNER JOIN image_tags ON images_tags.tags_id = image_tags.id WHERE images_tags.image_id = images.id AND image_tags.name = ?)");
            if(i + 1 < tags.size())
                q.append("AND");
        }
        if(query.getVoteSort() != ImageQuery.SortType.NONE || query.getNameSort() != ImageQuery.SortType.NONE) {
            q.append("ORDER BY ");
            if(query.getNameSort() != ImageQuery.SortType.NONE) {
                q.append("name " + (query.getNameSort() == ImageQuery.SortType.DESCEND ? "DESC" : "ASC"));
                if(query.getVoteSort() != ImageQuery.SortType.NONE)
                    q.append(" , ");
            }
            if(query.getVoteSort() != ImageQuery.SortType.NONE)
                q.append("v " + (query.getVoteSort() == ImageQuery.SortType.DESCEND ? "DESC" : "ASC"));
        }
        Query sql = entityManager.createNativeQuery(q.toString(), Image.class);
        for(int i = 0; i < tags.size(); i++)
            sql.setParameter(i + 1, tags.get(i));
        if(query.getStart() > 0)
            sql.setFirstResult(query.getStart());
        if(query.getLimit() > 0)
            sql.setMaxResults(query.getLimit());
        return (List<Image>) sql.getResultList();
    }
}
