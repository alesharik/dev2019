package com.alesharik.web.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "image_data")
public class ImageData {
    @Id
    @GeneratedValue
    private long id;
    private byte[] data;

    public ImageData() {
        data = null;
    }

    public ImageData(byte[] data) {
        this.data = data;
    }
}
