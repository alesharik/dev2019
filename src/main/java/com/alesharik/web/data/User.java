package com.alesharik.web.data;

import com.alesharik.web.auth.data.UserAuth;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Data
@ToString(exclude = {"auth", "images", "votes"})
@Table(name = "\"User\"", indexes = {@Index(columnList = "login")})
public class User {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 128, unique = true)
    private String login;

    @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
    private UserAuth auth;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    private Collection<Image> images;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    private Collection<ImageVote> votes;

    public User() {
        login = null;
        auth = null;
    }
}
