package com.alesharik.web.auth;

import com.alesharik.web.auth.data.UserAuth;
import com.alesharik.web.auth.data.UserAuthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserAuthRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAuth auth = repository.findUserAuthByUserLogin(username);
        if(auth == null)
            throw new UsernameNotFoundException("Cannot find user " + username);
        return new UserDetailsImpl(auth.getUser());
    }
}
