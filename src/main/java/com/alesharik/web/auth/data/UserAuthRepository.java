package com.alesharik.web.auth.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthRepository extends CrudRepository<UserAuth, Long> {

    @Query("select auth from UserAuth auth where auth.user.login = :login")
    UserAuth findUserAuthByUserLogin(@Param("login") String login);
}
