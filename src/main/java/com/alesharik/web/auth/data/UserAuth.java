package com.alesharik.web.auth.data;

import com.alesharik.web.auth.Security;
import com.alesharik.web.data.User;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class UserAuth {
    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    private User user;

    private String password;

    public UserAuth() {
        user = null;
        password = "";
    }

    public UserAuth(User user, String password) {
        this.user = user;
        this.password = Security.getPasswordEncoder().encode(password);
    }

    public void updatePassword(String password) {
        this.password = Security.getPasswordEncoder().encode(password);
    }
}
