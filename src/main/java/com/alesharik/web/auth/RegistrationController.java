package com.alesharik.web.auth;

import com.alesharik.web.auth.data.UserAuth;
import com.alesharik.web.auth.data.UserAuthRepository;
import com.alesharik.web.data.User;
import com.alesharik.web.data.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class RegistrationController {
    private final UserRepository userRepository;
    private final UserAuthRepository authRepository;

    @GetMapping("/register")
    public void register(@RequestParam("login") String login, @RequestParam("password") String password) {
        if(StringUtils.isEmpty(login) || StringUtils.isEmpty(password))
            throw new IllegalArgumentException("Login or password is empty");
        if(userRepository.countUserByLogin(login) > 0)
            throw new IllegalArgumentException("User already exists");

        log.debug("Registering user " + login + " with password " + password);
        User user = new User();
        user.setLogin(login);
        user = userRepository.save(user);
        UserAuth auth = new UserAuth(user, password);
        auth = authRepository.save(auth);
        user.setAuth(auth);
        userRepository.save(user);
    }
}
