package com.alesharik.web;

import com.alesharik.web.auth.data.UserAuth;
import com.alesharik.web.auth.data.UserAuthRepository;
import com.alesharik.web.data.User;
import com.alesharik.web.data.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Collections;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Main {
    private final UserRepository repository;
    private final UserAuthRepository authRepository;

    @Bean
    public CommandLineRunner init() {
        return args -> {
            log.info("Checking repository for users...");
            if(repository.count() == 0) {
                log.info("Adding admin...");
                User user = new User();
                user.setLogin("admin");
                user = repository.save(user);
                UserAuth admin = new UserAuth(user, "admin");
                admin = authRepository.save(admin);
                user.setAuth(admin);
                user.setImages(Collections.emptyList());
                user.setVotes(Collections.emptyList());
                repository.save(user);
                log.info("Admin added");
            }
            log.info("Started");
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
